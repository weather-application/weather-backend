"""
Main Flask Application Module

This module contains the lightweight flask app for this RESTful API service.
- Declares an ORM model with the help of SQLAlchemy
- For a lightweight RESTful API, it is easy to add routes for scalability.
"""

# Required imports / modules
import os
import json


from requests import Session
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from config import api_key, api_url
import processor


# Initializes the flask app
app = Flask(__name__)
app.config.from_object(os.environ["APP_SETTINGS"])
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

# Initializes the database connection with postgreSQL
db = SQLAlchemy(app)

# Allows alembic migrate to detect any changes from the ORM models
migrate = Migrate(app, db)


class CityModel(db.Model):
    """
    ORM model for City Table
    Defines the fields that are associated with a City object/data
    """

    # The name of the table inside the database
    __tablename__ = "city"

    # Primary Key ID
    id = db.Column(db.Integer, primary_key=True)

    # Fields that are unique to each City object
    name = db.Column(db.String(40))
    description = db.Column(db.String(50))
    temperature = db.Column(db.Float)
    wind_speed = db.Column(db.Float)
    country = db.Column(db.String(40), nullable=True)
    time = db.Column(db.String(20), nullable=True)

    def __repr__(self):
        """Returns a representation of a City object to identify it easily"""
        return f"{self.name}, {self.state}"


"""
Below are the routes defined for the flask application
"""


@app.route("/")
def index() -> str:
    """Index route which returns a greeting to the user"""
    return "Welcome to Weatherly's RESTful API service!"


@app.route("/city/<city_name>", methods=["POST", "GET"])
def get_city_data(city_name: str):
    """
    This route attempts to:
    - add a city to the database, if it does not exist
    - refresh the data that corresponds to the city, if it exists

    @params city_name
    The city name that we will use to make an API call to OpenWeatherMap
    """
    url = f"{api_url}/weather"

    # Initializes a processor for the incoming data
    data_processor = processor.Processor()

    # Creates a session for requests
    with Session() as session:
        # Headers and parameters that will be passed to the GET requests
        headers = {
            "content-type": "application/json",
        }
        parameters = {
            "q": city_name,
            "appid": api_key,
        }
        # Retrieves the data from OpenWeatherMapAPI
        resp = session.get(url, headers=headers, params=parameters)
        resp.raise_for_status()

        # load the data as a json and passed it to the processor
        data = json.loads(resp.text)
        collected_data = data_processor.add(data)

    collected_json = {
        "city_name": collected_data.name,
        "description": collected_data.description,
        "temperature": collected_data.temperature,
        "wind_speed": collected_data.wind_speed,
        "country": collected_data.country,
        "time": collected_data.time,
    }

    return collected_json


if __name__ == "__main__":
    app.run()
