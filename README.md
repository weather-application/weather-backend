# Weather Backend Application

This flask application is a RESTful API server which collects data from the OpenWeatherMap API and stores it in the database.
Routes are exposed to communicate with the client side front-end UI to display weather data with a city a user inputs.

## Technologies Used
- Python (Programming Language)
- Flask (Web Framework)
- PostgreSQL (Database)
- OpenWeatherMap (3rd Party API)
- Docker (Containerization)

## TODO
- [x] Sign up for OpenWeatherMap
- [x] Get Auth Key
- [x] Set up flask application
- [x] Set up PostgreSQL and SQLAlchemy
- [x] Write Dockerfile
