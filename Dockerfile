FROM python:3.9.9-alpine

# Upgrade pip
RUN pip install --upgrade pip

# make a local directory
RUN mkdir /app

# set "app" as the working directory from which CMD, RUN, ADD references
WORKDIR /app

# now copy all the files in this directory to /code
ADD . .

# install psycopg2 dependencies
RUN apk update
RUN apk add postgresql-dev gcc python3-dev musl-dev
RUN apk add build-base
# RUN apk --update --upgrade add gcc musl-dev jpeg-dev zlib-dev libffi-dev cairo-dev pango-dev gdk-pixbuf-dev

# install all dependencies and packages
RUN pip install -r requirements.txt

# set environment variables inside container
ENV API_KEY="3ab66bad541451921f55ffbcd9c892ec"
ENV FLASK_APP="app.py"
ENV APP_SETTINGS="config.ProductionConfig"
ENV DATABASE_URL="postgres://puwuwcwrtbnvuc:d367a4600be0e0c4fe672c40783d4d0026b0e3ac1a3e64b0724c84d85dfe80d3@ec2-3-230-82-215.compute-1.amazonaws.com:5432/de497ahpf3p9bu"

RUN flask db upgrade

CMD flask run -h 0.0.0.0 -p $PORT
