"""
Configuration Module

Contains all configuration for the flask application
"""

import os

basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    DEBUG = False
    TESTING = False
    CSRF_ENABLED = True
    SQLALCHEMY_DATABASE_URI = os.environ["DATABASE_URL"]
    API_KEY = os.environ["API_KEY"]
    API_URL = "http://api.openweathermap.org/data/2.5"


class ProductionConfig(Config):
    DEBUG = False


class StagingConfig(Config):
    DEVELOPMENT = True
    DEBUG = True


class DevelopmentConfig(Config):
    DEVELOPMENT = True
    DEBUG = True


class TestingConfig(Config):
    TESTING = True


api_key = Config.API_KEY
api_url = Config.API_URL
