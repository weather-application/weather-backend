"""
Processor Module

Module contains the Processor class which allows for the processing of data
received from external API. This class handles the communication with the database.
"""

import app
from datetime import datetime


def get_local_hour(dt, tz):
    """Helper function to process local time"""
    ts = dt + tz
    return datetime.utcfromtimestamp(ts).strftime("%H:%M")


class Processor:
    """This class defines methods or functions that can be used to send/process data with the database"""

    def __init__(self) -> None:
        """Initializer/Constructor for this class"""
        self.db = app.db
        self.city_model = app.CityModel

    def add(self, json_data):
        """Adds data to database if it does not exist"""
        city_name = json_data["name"]
        db_city = self.city_model.query.filter_by(name=city_name).first()

        new_data = self.city_model(
            name=city_name,
            description=json_data["weather"][0]["main"],
            temperature=json_data["main"]["temp"],
            wind_speed=json_data["wind"]["speed"],
            country=json_data["sys"]["country"],
            time=get_local_hour(json_data["dt"], json_data["timezone"]),
        )

        if db_city:
            # If it already exists, update the data that is inside the database
            print(
                f"{city_name} is already recorded inside the database. Refreshing data."
            )
            self._refresh(db_city, json_data)
        else:
            # Addition of new data i`nto the database.
            self.db.session.add(new_data)
            self.db.session.commit()

        return new_data

    def _refresh(self, existing_city, json_data):
        """This function focuses on updating the old data with the new incoming data"""
        existing_city.temperature = json_data["main"]["temp"]
        existing_city.description = json_data["weather"][0]["main"]
        existing_city.wind_speed = json_data["wind"]["speed"]
        existing_city.time = get_local_hour(json_data["dt"], json_data["timezone"])
        self.db.session.commit()

    def delete(self, city_name):
        """Deletes specified data inside database"""
        self.city_model.query.filter_by(name=city_name).delete()
        self.db.session.commit()
